package stoiovkar.com.youtubeplayer;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SeekBar;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayer.OnInitializedListener;
import com.google.android.youtube.player.YouTubePlayerView;

public class YouTubeView extends YouTubeBaseActivity implements OnInitializedListener {

    private YouTubePlayerView playerView;
    private Button playButton;
    private Button pauseButton;
    private YouTubePlayer player;
    private SeekBar seekBar;
    private String GOOGLE_API_KEY = "AIzaSyCEsPvzY-fUnPHN3Hd1tQcsJrKBeLb8l88";
    private String YOUTUBE_VIDEO_ID = "svnAD0TApb8";
    private String YOUTUBE_PLAYLIST = "PLjRHXbJPAFJx_4VCnlnC59ukQrmdqK4Im";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_you_tube_view);
        playerView = (YouTubePlayerView) findViewById(R.id.youtube_player);
        playerView.initialize(GOOGLE_API_KEY,this);
        playButton = (Button) findViewById(R.id.play_button);
        seekBar = (SeekBar) findViewById(R.id.seekBar);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if (player != null) {
                    player.seekToMillis(i);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (player != null) {
                    player.play();
                }
            }
        });
        pauseButton = (Button) findViewById(R.id.pause_button);
        pauseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (player != null) {
                    player.pause();
                }
            }
        });

        LinearLayout linear = (LinearLayout) findViewById(R.id.linear_layout);
        linear.bringToFront();
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean wasRestored) {
        if(null == youTubePlayer) return;
        player = youTubePlayer;

        // Start buffering
        if (!wasRestored) {
            youTubePlayer.cueVideo(YOUTUBE_VIDEO_ID);
            player.play();

        }

        player.setPlayerStyle(YouTubePlayer.PlayerStyle.CHROMELESS);
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

    }
}
